package com.example.proyecttienbox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;

import java.util.ArrayList;
import java.util.List;

public class Productos extends AppCompatActivity {

    List<ListElement> elements;
    //La declaramos de forma global

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        init(); //lo convierte en método
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;

    }

    public void init(){
        elements = new ArrayList<>(); //Instancia del objeto elements
        elements.add(new ListElement("#775447","Producto 2","Colombia","Activo"));
        elements.add(new ListElement("#607d8b","Producto 3","Colombia","Activo"));
        elements.add(new ListElement("#03a9f4","Producto 4","Colombia","Activo"));
        elements.add(new ListElement("#009688","Producto 5","Colombia","Activo"));

        //Declaramos el ListAdapter y recibe una lista y el context = de donde viene
        ListAdapter listAdapter = new ListAdapter(elements,this);
        //DEclaramos el recyclerView
        RecyclerView recyclerView = findViewById(R.id.listRecyclerView);
        //Movemos unos parametros en verdadero
        recyclerView.setHasFixedSize(true);
        // Listado lineal setLayaoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(listAdapter);

    }

}