package com.example.proyecttienbox.persistencia;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

public class DbUsuarios extends DbHelper{

    Context context;

    // CRUD
    // C = CREAR
    // R = LEER
    // U = ACTUALIZAR
    // D = ELMINIAR

    // Constructor
    public DbUsuarios(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    // long : es de tipo entero de mayor tamaño

    public Boolean insertarUsuario(String nomusuario, String contrasena, String correo){

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nomusuario", nomusuario);
        values.put("contrasena", contrasena);
        values.put("correo", correo);

        long result =db.insert(TABLE_USERS,null,values);
        if (result == -1) return  false;
        else
            return true;


    }

    public Boolean checkusuario(String nomusuario){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE nomusuario =?",new String[] {nomusuario});
        if(cursor.getCount()>0)
            return true;
        else
            return false;


    }

    public Boolean checkcontrasena(String nomusuario,String contrasena){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE nomusuario =? and contrasena =?",new String[] {nomusuario,contrasena});
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
}
