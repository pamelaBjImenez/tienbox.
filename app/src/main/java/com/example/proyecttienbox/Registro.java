package com.example.proyecttienbox;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proyecttienbox.persistencia.DbHelper;
import com.example.proyecttienbox.persistencia.DbUsuarios;

public class Registro extends AppCompatActivity {

    EditText username1, password1, repassword, correo,nombreid;
    Button btnRegistro,volverSingin;
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        username1 = findViewById(R.id.username1);
        password1 = findViewById(R.id.password);
        repassword = findViewById(R.id.repassword);
        correo = findViewById(R.id.correo);
        nombreid = findViewById(R.id.nombreid);

        btnRegistro = findViewById(R.id.btnRegistro);
        volverSingin =findViewById(R.id.volverSingin);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user =username1.getText().toString();
                String pass =password1.getText().toString();
                String repass = repassword.getText().toString();
                String email = correo.getText().toString();
                String nameid =nombreid.getText().toString();

                if(TextUtils.isEmpty(user) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(repass) || TextUtils.isEmpty(email) || TextUtils.isEmpty(nameid))
                    Toast.makeText(Registro.this,"Requiere llenar los campos",Toast.LENGTH_SHORT).show();
                else
                    if(pass.equals(repass)){
                        Boolean checkuser = DB.checkusuario(user);
                        if(checkuser == false){
                            Boolean insert = DB.insertarUsuario(user,pass,email);
                            if(insert == true){
                                Toast.makeText(Registro.this,"Refistrado correctamente", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(Registro.this,"Registro fallido", Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(Registro.this,"Registro exitoso", Toast.LENGTH_SHORT).show();
                        }
                    }
            }
        });

        volverSingin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //btnRegistro.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
              //  DbHelper dbHelper = new DbHelper(Registro.this);
                //SQLiteDatabase db = dbHelper.getWritableDatabase();
                //Validación
                //if(db != null){
                  //  Toast.makeText(Registro.this,"BASE DE DATOS CREADA",Toast.LENGTH_LONG).show();
                //}else{
                  //  Toast.makeText(Registro.this,"ERROR AL CREAR BASE DE DATOS",Toast.LENGTH_LONG).show();
               // }
            //}
        //});
    }
}