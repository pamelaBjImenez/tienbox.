package com.example.proyecttienbox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyecttienbox.persistencia.DbUsuarios;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    EditText username, password;
    Button loginbtn, btnRegis; // Declaramos de forma global nuestra variable Button
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide(); // Eliminamos la barrar superior

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);
        //MaterialButton btnRegis = (MaterialButton) findViewById(R.id.btnRegis); // NEW

        // Conetext = La clase en la que estamos ubicados ahora mismo
        DB = new DbUsuarios(this);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username.getText().toString(); // Tomar los datos que agregó el usuario
                String pass = password.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                    Toast.makeText(MainActivity.this, "Todos los espacios son requeridos", Toast.LENGTH_SHORT).show();
                else {
                    Boolean checkuserpass = DB.checkcontrasena(user, pass);
                    if (checkuserpass == true) {
                        // Toast.makeText(MainActivity.this, "Login correcto", Toast.LENGTH_SHORT).show();
                        Dialogo d = new Dialogo(MainActivity.this, "Aqui va el titulo", "Hola gente", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getApplicationContext(), CardsItems.class);
                                startActivity(intent);
                            }
                        });
                    } else {
                        Toast.makeText(MainActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cambiamos de layout/Vista
                Intent intent = new Intent(getApplicationContext(), Registro.class);
                startActivity(intent);
            }
        });
    }
}


    //private Dialog dialog;
    //private Button ShowDialog;
   // EditText username, password;
    //Button loginbtn;



    //@Override
    //protected void onCreate(Bundle savedInstanceState) {
      //  super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //getSupportActionBar().hide(); //ELIMINA LA BARRA SUPERIOR

        //username = findViewById(R.id.username);
        //password = findViewById(R.id.password);

      //  MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);

        //loginbtn.setOnClickListener(new View.OnClickListener() {
          //@Override
            //public void onClick(View view) {
              //  DbUsuarios dbUsuarios= new DbUsuarios(MainActivity.this);
                //long id = dbUsuarios.insertarUsuario(username.getText().toString(),password.getText().toString());
//
  //              if(id)
    //        }
      //  });



        //ShowDialog = findViewById(R.id.loginbtn);




        //MaterialButton loginbtn =(MaterialButton) findViewById(R.id.loginbtn);
        //Dialog dialog;

        // TextView username= (TextView)  findViewById(R.id.username);
        // TextView password = (TextView) findViewById(R.id.password);

        //dialog = new Dialog(MainActivity.this); // Instancia del obejto Dialog
        //dialog.setContentView(R.layout.custom_dialog);
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
        //    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.backgroung2));
//
        //}

        //dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.backgroung2));
        //dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        //dialog.setCancelable(false);

        //Button Okey = dialog.findViewById(R.id.btn_okay);
        //Button Cancel = dialog.findViewById(R.id.btn_cancel);

        //Okey.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
//
  //              Toast.makeText(MainActivity.this, "Okay", Toast.LENGTH_SHORT).show();
    //            dialog.dismiss();
      //      }
        //});

        //Cancel.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
//
  //              Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
    //            dialog.dismiss();
      //      }
        //});


        //ShowDialog.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
//
  //              dialog.show(); // Showing the dialog here
    //        }
      //  });

//        dialog_btn.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                Snackbar snackbar = Snackbar.makeText(view,"Primer snackbar",Snackbar.LENGHT_LONG);
//                Snackbar.show();
//
//            }
//        });


        //--------comentado---
        //loginbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//           public void onClick(View v) {
//                if (username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
//                    Toast.makeText(MainActivity.this,"Usuario registrado",Toast.LENGTH_SHORT).show();
//
//                }else
//                    Toast.makeText(MainActivity.this,"Usuario no registrato",Toast.LENGTH_SHORT).show();
//
//            }
//        });
    //}

    //public void regis(View m){
      //  Intent cambiar = new Intent(MainActivity.this,Registro.class);
        //startActivity(cambiar);
    //}


